<?php
# Page variables

$siteRoot = "../";
$siteRootUrl = "../";

$theme = "default";

$title = "Log in";

# Page

include $siteRoot."/resources/header/header.php";
include $siteRoot."/resources/login/login.php";
include $siteRoot."/resources/footer/footer.php";

?>