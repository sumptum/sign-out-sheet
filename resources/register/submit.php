<?php

$dbU = "signoutsheet";
$dbP = "dmq4qRmM9hvUuSEs";
$dbA = "127.0.0.1";
$dbD = "signoutsheet";

if ((strlen($_POST["forename"])>1) && (preg_match('/^[a-zA-Z]+$/', $_POST["forename"])) && (preg_match('/^[a-zA-Z]+$/', $_POST["surname"])) && (strlen($_POST["surname"])>1) && (strlen($_POST["password"])>1) && ((substr_count($_POST["email"],'@') == 1) && (strpos($_POST["email"], "@.")===false) && (strpos($_POST["email"], ".@")===false) && (strpos($_POST["email"], "..")===false) && (gethostbyname(explode("@", $_POST["email"])[1]) != explode("@", $_POST["email"])[1]))) {
    try {
        $conn = new PDO("mysql:host=$dbA;dbname=$dbD", $dbU, $dbP);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$stmt = $conn->prepare("SELECT email FROM users WHERE email LIKE :email"); 
		$stmt->bindParam(':email', $email);
		
		$email = strtolower($_POST["email"]);
		
        $stmt->execute();
		
		$uniqueEntry = true;
        foreach ($stmt as $row) {
			$uniqueEntry = false;
			echo "Record already exists!";
		}
		
		if ($uniqueEntry) {
			// prepare sql and bind parameters
            $stmt = $conn->prepare("INSERT INTO users (email, password, forename, surname, form) 
            VALUES (:email, :password, :forename, :surname, :form)");
            $stmt->bindParam(':email', $email);
            $stmt->bindParam(':password', $password);
            $stmt->bindParam(':forename', $forename);
    	    $stmt->bindParam(':surname', $surname);
    	    $stmt->bindParam(':form', $form);
    	    
    	    $email = strtolower($_POST["email"]);
    	    $password = hash("md5", $_POST["password"]);
    	    $forename = $_POST["forename"];
    	    $surname = $_POST["surname"];
    	    $form = $_POST["form"];
    	    
    	    $stmt->execute();
    	    
		    header("Location: ../../");
		} else {
			echo "Error: That email is already linked to an account. If you believe this is erroneous, please contact the site administrator for assistance";
		}
		
    } catch(PDOException $e) {
    	echo "Error: " . $e->getMessage();
    }
} else {
	if (gethostbyname(explode("@", $_POST["email"])[1]) == explode("@", $_POST["email"])[1]) {
		echo "Looks like an invalid email address, please try again";
	} else {
	    echo "Error: Submitted data failed to validate. Are you using a browser from the early 90s? Or did you try bypassing the JS check? You did, didn't you. I caught that. Nice try.";
	}
}

?>
