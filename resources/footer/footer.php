    <script>
      function init() {
        var a=document.getElementsByTagName("a");
        for(var i=0;i<a.length;i++) {
          a[i].onclick=function() {
            window.location=this.getAttribute("href");
            return false;
          }
        }
      }
	  function validateRegister() {
		if (document.getElementById("forename").value.length < 2) {
		  var validateRegister_forename = false;
		  document.getElementById("validateRegister_forename_e").innerHTML = "Please enter a forename";
		} else {
		  if (!(/^[a-zA-Z]+$/.test(document.getElementById("forename").value))) {
			var validateRegister_forename = false;
		    if (/\d/.test(document.getElementById("forename").value)) { document.getElementById("validateRegister_forename_e").innerHTML = "Forename cannot contain numbers"; } else { document.getElementById("validateRegister_forename_e").innerHTML = "forename cannot contain symbols"; }
		  } else {var validateRegister_forename = true; document.getElementById("validateRegister_forename_e").innerHTML = "";}
		}
		if (document.getElementById("surname").value.length < 2) {
		  var validateRegister_surname = false;
		  document.getElementById("validateRegister_surname_e").innerHTML = "Please enter a surname";
		} else {
		  if (!(/^[a-zA-Z]+$/.test(document.getElementById("surname").value))) {
			var validateRegister_surname = false;
			if (/\d/.test(document.getElementById("surname").value)) { document.getElementById("validateRegister_surname_e").innerHTML = "Surname cannot contain numbers"; } else { document.getElementById("validateRegister_surname_e").innerHTML = "Surname cannot contain symbols"; }
		  } else {var validateRegister_surname = true; document.getElementById("validateRegister_surname_e").innerHTML = "";}
		}
		/*if (document.getElementById("form").value.length < 2 || document.getElementById("form").value.length > 6) {
	      var validateRegister_form = false;
		  document.getElementById("validateRegister_form_e").innerHTML = "Please enter a valid form";
		} else {
		  if (/^\d+$/.test(document.getElementById("form").value)) {
			var validateRegister_form = false;
		    document.getElementById("validateRegister_form_e").innerHTML = "Form must contain initials";
		  } else {
			var validateRegister_form = true;
		    document.getElementById("validateRegister_form_e").innerHTML = "";
		  }
		}*/
		if (document.getElementById("form").value!="NOTHINGSELECTED") {
			var validateRegister_form = true;
		    document.getElementById("validateRegister_form_e").innerHTML = "";
		} else {
			var validateRegister_form = false;
		    document.getElementById("validateRegister_form_e").innerHTML = "You must choose a form";
		}
		if (~document.getElementById("email").value.indexOf('@') && document.getElementById("email").value.split("@").length==2 && document.getElementById("email").value.split("@")[1].split(".").length >=1 && !(~document.getElementById("email").value.indexOf('@.')) && !(~document.getElementById("email").value.indexOf('.@')) && !(~document.getElementById("email").value.indexOf('..'))) {
		  var validateRegister_email = true;
		  document.getElementById("validateRegister_email_e").innerHTML = "";
		} else {
		  var validateRegister_email = false;
		  document.getElementById("validateRegister_email_e").innerHTML = "That email was not recognised";
		}
		if (document.getElementById("password").value < 2) {
		  validateRegister_password = false;
		  document.getElementById("validateRegister_password_e").innerHTML = "Please enter a password";
		} else { var validateRegister_password = true; document.getElementById("validateRegister_password_e").innerHTML = ""; }
		if (validateRegister_email && validateRegister_forename && validateRegister_surname && validateRegister_form && validateRegister_password) {
			return true;
		} else {
			return false;
		}
	  }
	  function validateLogin() {
		if (~document.getElementById("email").value.indexOf('@') && document.getElementById("email").value.split("@").length==2 && document.getElementById("email").value.split("@")[1].split(".").length >=1 && !(~document.getElementById("email").value.indexOf('@.')) && !(~document.getElementById("email").value.indexOf('.@')) && !(~document.getElementById("email").value.indexOf('..'))) {
		  var validateLogin_email = true;
		  document.getElementById("validateLogin_email_e").innerHTML = "";
		} else {
		  var validateLogin_email = false;
		  document.getElementById("validateLogin_email_e").innerHTML = "That email was not recognised";
		}
		if (document.getElementById("password").value < 2) {
		  validateLogin_password = false;
		  document.getElementById("validateLogin_password_e").innerHTML = "Please enter a password";
		} else { var validateLogin_password = true; document.getElementById("validateLogin_password_e").innerHTML = ""; }
		if (validateLogin_email && validateLogin_password) {
			return true;
		} else {
			return false;
		}
	  }
      function signOutHandler() {
        var signOutReason = prompt("Why are you signing out?","Co-op");
        if (signOutReason.length>2) {
          if (confirm("Sign out now, with reason: "+signOutReason+" ?")) {
            window.location.href = './signout/?r='+signOutReason;
          }
        } else {
          alert("Sorry, that wasn't accepted as a reason. Please try again.");
        }
      }
	  function onUpdateReady() {
        window.applicationCache.update();
      }
      window.applicationCache.addEventListener('updateready', onUpdateReady);
      if(window.applicationCache.status === window.applicationCache.UPDATEREADY) {
        onUpdateReady();
      }
	</script>
  </body>
</html>