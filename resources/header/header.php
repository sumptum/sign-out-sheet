<?php if ($manifest==true) { ?>
  <html lang="en" manifest="<?php echo $siteRootUrl; ?>/offline.appcache">
<?php } else { ?>
<html>
<?php } ?>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="apple-touch-icon" href="<?php echo $siteRootUrl; ?>resources/images/icon.png">
    <title><?php echo $title ?></title>
    <style><?php include $siteRoot."resources/theme/all.php"; ?></style>
    <style><?php include $siteRoot."resources/theme/".$theme."/stylesheet.php"; ?></style>
  </head>
  <body onload="init()">
    <div id="header"><center><a href="<?php echo $siteRootUrl; ?>"><img src="<?php echo $siteRootUrl; ?>resources/images/Header/sixthformlogo.png"></img></a></center></div>
    <div id="contentWrapper">