body {
  font-family: sans-serif;
}
input, textarea, select {
  -webkit-appearance: none;
  -webkit-border-radius: 0px;
  background-color: white;
  border: #444 1px solid;
}
#registerForm {
  position: relative;
  top: 24px;
  left: 50%;
  width: 320px;
  margin-left: -160px;
}
#registerForm input, #registerForm select {
  position: relative;
  padding: 8px;
  width: 90%;
  left: 50%;
  -moz-transform: translateX(-50%);
  -webkit-transform: translateX(-50%);
  transform: translateX(-50%);
  //margin-top: 8px;
  padding-top: 16px;
  padding-bottom: 16px;
}
#registerForm .beginning {
  border-top-right-radius: 4px;
  border-top-left-radius: 4px;
}
#registerForm .end {
  border-bottom-right-radius: 4px;
  border-bottom-left-radius: 4px;
}
#loginForm {
  position: relative;
  top: 24px;
  left: 50%;
  width: 320px;
  margin-left: -160px;
}
#loginForm input, #loginForm select {
  position: relative;
  padding: 8px;
  width: 90%;
  left: 50%;
  -moz-transform: translateX(-50%);
  -webkit-transform: translateX(-50%);
  transform: translateX(-50%);
  margin-top: 8px;
  padding-top: 16px;
  padding-bottom: 16px;
  border-radius: 4px;
}
#landingMenu {
  position: relative;
  top: 24px;
  left: 50%;
  width: 320px;
  margin-left: -160px;
}
#landingMenu input {
  position: relative;
  padding: 8px;
  width: 90%;
  left: 50%;
  -moz-transform: translateX(-50%);
  -webkit-transform: translateX(-50%);
  transform: translateX(-50%);
  margin-top: 8px;
  padding-top: 16px;
  padding-bottom: 16px;
  border-radius: 4px;
}
#settingsMenu {
  position: relative;
  top: 24px;
  left: 50%;
  max-width: 320px;
  width: 90%;
  left: 50%;
  -moz-transform: translateX(-50%);
  -webkit-transform: translateX(-50%);
  transform: translateX(-50%);
}
#settingsMenu div {
  position: relative;
  border: #666666 1px solid;
  margin-top: 8px;
  padding: 16px;
  width: 90%;
  left: 50%;
  -moz-transform: translateX(-50%);
  -webkit-transform: translateX(-50%);
  transform: translateX(-50%);
}
#settingsMenu .cat {
  color: #aaa;
}
#settingsMenu input {
  position: relative;
  padding: 8px;
  margin-top: 8px;
  width: 100%;
  left: 50%;
  -moz-transform: translateX(-50%);
  -webkit-transform: translateX(-50%);
  transform: translateX(-50%);
}