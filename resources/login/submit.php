<?php

session_start();

if(strpos(strtolower($_SERVER['HTTP_USER_AGENT']),"apple")) //to prevent cookies for non-apple devices
{
    $cookieLifetime = 365 * 24 * 60 * 60; // A year in seconds
    setcookie("ses_id",session_id(),time()+$cookieLifetime);
}

$dbU = "signoutsheet";
$dbP = "dmq4qRmM9hvUuSEs";
$dbA = "127.0.0.1";
$dbD = "signoutsheet";

if ((strlen($_POST["password"])>1) && ((substr_count($_POST["email"],'@') == 1) && (strpos($_POST["email"], "@.")===false) && (strpos($_POST["email"], ".@")===false) && (strpos($_POST["email"], "..")===false) && (gethostbyname(explode("@", $_POST["email"])[1]) != explode("@", $_POST["email"])[1]))) {
    try {
        $conn = new PDO("mysql:host=$dbA;dbname=$dbD", $dbU, $dbP);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$stmt = $conn->prepare("SELECT * FROM users WHERE email LIKE :email"); 
		$stmt->bindParam(':email', $email);
		
		$email = strtolower($_POST["email"]);
		
        $stmt->execute();
		
		$emailRegistered = false;
        foreach ($stmt as $row) {
            $userHashedPw = $row["password"];
            $userForename = $row["forename"];
            $userSurname = $row["surname"];
            $userForm = $row["form"];
            $userEmail = $row["email"];
            $userID = $row["userid"];
			$emailRegistered = true;
		}
		
		if ($emailRegistered && (md5($_POST["password"])==$userHashedPw)) {
            $_SESSION["Forename"] = $userForename;
            $_SESSION["Surname"] = $userSurname;
            $_SESSION["Form"] = $userForm;
            $_SESSION["Email"] = $userEmail;
            $_SESSION["UserID"] = $userID;
		    header("Location: ../../");
		} else {
			echo "Error: Password is incorrect.";
		}
    } catch(PDOException $e) {
    	echo "Error: " . $e->getMessage();
    }
} else {
	if (gethostbyname(explode("@", $_POST["email"])[1]) == explode("@", $_POST["email"])[1]) {
		echo "Looks like an invalid email address, please try again";
	} else {
	    echo "Error: Submitted data failed to validate. Are you using a browser from the early 90s? Or did you try bypassing the JS check? You did, didn't you. I caught that. Nice try.";
	}
}

?>
