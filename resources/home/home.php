
<?php

if (isset($_SESSION["Forename"])) {
  $tableName = date("D_d_F_Y");
  
  $dbU = "signoutsheet";
  $dbP = "dmq4qRmM9hvUuSEs";
  $dbA = "127.0.0.1";
  $dbD = "signoutsheet";
  
  $mysqliLink = mysqli_connect($dbA,$dbU,$dbP);
  mysqli_select_db($mysqliLink, $dbD);
  
  $val = mysqli_query($mysqliLink, "select 1 from `".$tableName."` LIMIT 1");
  
  if ($val !== FALSE) {
    try {
      $conn = new PDO("mysql:host=$dbA;dbname=$dbD", $dbU, $dbP);
      // set the PDO error mode to exception
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
      $stmt = $conn->prepare("SELECT * FROM `".$tableName."` WHERE userid LIKE :userid ORDER BY outTime DESC LIMIT 1"); 
      $stmt->bindParam(':userid', $userID);
      
      $userID = $_SESSION["UserID"];
      
      $stmt->execute();
    
      $emailRegistered = false;
      foreach ($stmt as $row) {
        if ($row["inTime"]!="0000-00-00 00:00:00") {
          $userStatus = "signedIn";
        } else {
          $userStatus = "signedOut";
        }
      }
    } catch(PDOException $e) {
      echo "Error: " . $e->getMessage();
    }
  }
}
?>

<div id="landingMenu">
  <?php if (isset($_SESSION["Forename"])) { ?>
    <?php if ($userStatus!="signedOut") { ?>
      <p align="center">Hi <?php echo $_SESSION["Forename"]; ?>! You're currently signed in.</p>
      <input type="submit" value="Sign Out" onclick="signOutHandler()"></input>
      <a href="settings"><input type="submit" value="Settings"></input></a>
    <?php } else { ?>
      <p align="center">Hi <?php echo $_SESSION["Forename"]; ?>! You're currently signed out.</p>
      <a href="signin"><input type="submit" value="Sign In"></input></a>
      <a href="settings"><input type="submit" value="Settings"></input></a>
    <?php } ?>
  <?php } else { ?>
    <p align="center">Hi! You're not currently signed in on this device. Please register or log in below.</p>
    <a href="register"><input type="submit" value="Register"></input></a>
    <a href="login"><input type="submit" value="Log In"></input></a>
  <?php } ?>
</div>