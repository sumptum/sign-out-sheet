<?php

if($_COOKIE['ses_id']){
    session_id($_COOKIE['ses_id']);
}
session_start();

# Page variables

$siteRoot = "./";
$siteRootUrl = "./";

$theme = "default";

$title = "Sign In / Out Sheet";

# Page

include $siteRoot."/resources/header/header.php";
include $siteRoot."/resources/home/home.php";
include $siteRoot."/resources/footer/footer.php";

?>