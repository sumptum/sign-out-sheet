<?php
# Page variables

$siteRoot = "../";
$siteRootUrl = "../";

$theme = "default";

$title = "Register";

# Page

$manifest=true;
include $siteRoot."/resources/header/header.php";
include $siteRoot."/resources/register/register.php";
include $siteRoot."/resources/footer/footer.php";
$manifest=false;
?>