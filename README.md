# README #

Welcome to the digital sign in / out project page

### Project summary ###

* A digital replacement for a physical register recording on-site presence of pupils via timestamped signatures of people leaving and returning to the site
* Current version :: 1.2.2.1

> Versioning scheme: *major.minor[.maintenance[.build]]*, where major signifies a large-scale project alteration such as alpha-->beta, minor signifies a feature addition / modification, maintenance signifies a bugfix / improvement and build signifies a release for a specific use case