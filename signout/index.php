<?php
if($_COOKIE['ses_id']){
    session_id($_COOKIE['ses_id']);
}
session_start();

if (isset($_SESSION["Forename"])&&strlen($_GET["r"])>2) {
  $tableName = date("D_d_F_Y");
  
  $dbU = "signoutsheet";
  $dbP = "dmq4qRmM9hvUuSEs";
  $dbA = "127.0.0.1";
  $dbD = "signoutsheet";
  
  $mysqliLink = mysqli_connect($dbA,$dbU,$dbP);
  mysqli_select_db($mysqliLink, $dbD);
  
  $val = mysqli_query($mysqliLink, "select 1 from `".$tableName."` LIMIT 1");
  
  if ($val === FALSE) {
    try {
      $conn = new PDO("mysql:host=$dbA;dbname=$dbD", $dbU, $dbP);
      $conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
      $sql ="CREATE table $tableName(
      eventid INT( 11 ) AUTO_INCREMENT PRIMARY KEY,
      userid INT ( 11 ) NOT NULL,
      reason TEXT NOT NULL,
      outTime DATETIME NOT NULL,
      inTime DATETIME NOT NULL);" ;
      $conn->exec($sql);
    } catch(PDOException $e) {
      echo $e->getMessage();
    }
  }
  try {
     $conn = new PDO("mysql:host=$dbA;dbname=$dbD", $dbU, $dbP);
     // set the PDO error mode to exception
     $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    $stmt = $conn->prepare("SELECT * FROM `".$tableName."` WHERE userid LIKE :userid ORDER BY outTime DESC LIMIT 1"); 
    $stmt->bindParam(':userid', $userID);
    
    $userID = $_SESSION["UserID"];
    
    $stmt->execute();
    
    $noEntryInTable = true;
    $alreadySignedOut = false;
    foreach ($stmt as $row) {
      $noEntryInTable = false;
      if ($row["inTime"]=="0000-00-00 00:00:00") {
        $alreadySignedOut = true;
        echo "Uh oh! Looks to me like you're already signed out. Please contact an administrator if this is an error<br/>";
        echo $row["inTime"];
      }
    }
    if ($noEntryInTable||!$alreadySignedOut) {
      try {
          $conn = new PDO("mysql:host=$dbA;dbname=$dbD", $dbU, $dbP);
          $conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
          
          $stmt = $conn->prepare("INSERT INTO `".$tableName."` (userid, reason, outTime)
          VALUES (:userid, :reason, :outTime)");
          $stmt->bindParam(':userid', $userid);
          $stmt->bindParam(':reason', $reason);
          $stmt->bindParam(':outTime', $outTime);
          
          $userid = $_SESSION["UserID"];
          $reason = $_GET["r"];
          $outTime = date("Y-m-d H:i:s");
          
          $stmt->execute();
          
          header("Location: ../");
        } catch(PDOException $e) {
          echo "Error: " . $e->getMessage();
        }
    }
  } catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
  }
  
} else {
  header("Location: ../");
}

?>