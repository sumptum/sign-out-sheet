<?php

if($_COOKIE['ses_id']){
    session_id($_COOKIE['ses_id']);
}
session_start();

# Page variables

$siteRoot = "../";
$siteRootUrl = "../";

$theme = "default";

$title = "Settings";

# Page

include $siteRoot."/resources/header/header.php";
include $siteRoot."/resources/settings/settings.php";
include $siteRoot."/resources/footer/footer.php";

?>